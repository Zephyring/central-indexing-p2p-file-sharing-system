package indexer;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.HashSet;

public interface IndexerInterface extends Remote {
	public void regAddr(int id, String ip) throws RemoteException;

	public String getAddr(int id) throws RemoteException;

	public void registry(int id, String filename) throws RemoteException;

	public HashSet<Integer> search(String filename) throws RemoteException;

	public void remove(int id, String filename) throws RemoteException;

}

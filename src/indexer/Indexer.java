package indexer;

import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;
import java.util.HashSet;
import util.Util;

import com.google.common.collect.HashMultimap;

public class Indexer extends UnicastRemoteObject implements IndexerInterface {

	/**
	 * The Indexer stores files information for each peer
	 * Allow peer to register, search, remove file information
	 */
	private static final long serialVersionUID = 6980331069362303612L;
	private HashMultimap<String, Integer> index;
	private HashMap<Integer, String> ipmap;

	/**
	 * Once Indexer instance is created, it creates index, which stores file information
	 * and ipmap, which stores ip information for each peer.
	 * @throws RemoteException
	 */
	public Indexer() throws RemoteException {
		super(0);
		index = HashMultimap.create();
		ipmap = new HashMap<Integer, String>();
	}
	
	/**
	 * The start function of Indexer allows indexer to register to RMI server
	 * @throws Exception
	 */
	public void start() throws Exception {
		this.register();
		System.out.println("Indexer started.");
	}
	
	/**
	 * It allows peer to register ip address, port to indexer
	 * @param id peer id
	 * @param ipport ipaddress and port number of peer
	 */
	public synchronized void regAddr(int id, String ipport) {
		ipmap.put(id, ipport);
	}

	/**
	 * It allows peer to lookup ip address and port number by peer id
	 * @param id peer id
	 * @return ipport address and port of a peer
	 */
	public String getAddr(int id) {
		return ipmap.get(id);
	}
	
	/**
	 * It allows peer to register file to indexer by peer id
	 * @param id peer id
	 * @param filename name of file the peer wants to register for
	 */
	public synchronized void registry(int id, String filename) {
		index.put(filename, id);
		System.out.println("Peer " + id + " registered file " + filename);
	}

	/**
	 * It allows peer to search which peers contain the file
	 * @param filename the file peer wants to search for
	 * @return hashset a set of peer ids that contain the file
	 */
	public HashSet<Integer> search(String filename) {
		return new HashSet<Integer>(index.get(filename));
	}

	/**
	 * It allows peer to remove the file from indexer by peer id
	 * @param id peer id
	 * @param filename name of file peer wants to remove
	 */
	public synchronized void remove(int id, String filename) {
		index.remove(filename, id);
		System.out.println("Peer " + id + " removed file " + filename);
	}

	/**
	 * It allows indexer to register to RMI server
	 * It first read basic port, ip address information from a configuration file
	 * and then register.
	 */
	private void register() {
		try {
			LocateRegistry.createRegistry(Integer.parseInt(Util
					.readProperties("indexerPort")));
			System.out.println("Indexer RMI registry created.");
			Naming.rebind(
					"//" + Util.readProperties("indexerIp") + ":"
							+ Util.readProperties("indexerPort") + "/Indexer",
					this);
			System.out.println("Indexer bound to //"
					+ Util.readProperties("indexerIp") + ":"
					+ Util.readProperties("indexerPort") + "/Indexer");
		} catch (Exception e) {
			System.out.println("Indexer RMI already exists.");
			System.out.println("Indexer Exit.");
			e.printStackTrace();
			System.exit(0);
		}
	}

	public static void main(String[] args) throws Exception {
		Indexer indexer = new Indexer();
		indexer.start();
	}

}

package peer;

import indexer.IndexerInterface;

import java.io.File;
import java.io.RandomAccessFile;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Scanner;
import java.util.Timer;
import java.util.TimerTask;

import util.Util;

public class Peer extends UnicastRemoteObject implements PeerInterface {

	/**
	 * The peer manages local files shared directory
	 * download file from other peers
	 * register, search and remove file information from indexer
	 */
	private static final long serialVersionUID = -2328920647527329891L;

	private int id;
	private HashSet<String> filenames;
	private IndexerInterface indexer;

	private RunDownload runDownload;

	/**
	 * The constructor first looks up indexer in RMI server and
	 * creates a local cache for files list in the indexer
	 * @param id peer id
	 * @throws Exception
	 */
	public Peer(int id) throws Exception {
		super();
		this.id = id;
		this.indexer = (IndexerInterface) Naming.lookup("//"
				+ Util.readProperties("indexerIp") + ":"
				+ Util.readProperties("indexerPort") + "/Indexer");
		System.out.println("Looking up //" + Util.readProperties("indexerIp")
				+ ":" + Util.readProperties("indexerPort") + "/Indexer");
		this.filenames = new HashSet<String>();
		this.updateIndexer();
	}

	/**
	 * The start function of peer.
	 * It registers peer to RMI server and 
	 * starts a schedule every 5 seconds to check local files list if any change happens
	 * @throws Exception
	 */
	public void start() throws Exception {
		this.register();
		this.scheduler(5);// check update every 5 seconds;
		System.out.println("Peer " + getId() + " started.");
	}

	/**
	 * It allows peer to get content of a file based on filename
	 * It's the building block for download.
	 * @param filename name of file
	 * @return buffer byte array of file content
	 */
	public byte[] obtain(String filename) throws RemoteException {
		try {
			String path = "p" + this.id + "/" + filename;
			RandomAccessFile f = new RandomAccessFile(path, "r");
			byte[] buffer = new byte[(int) f.length()];
			f.read(buffer);
			f.close();
			return buffer;
		} catch (Exception e) {
			System.out.println("File " + filename + " not found in Peer "
					+ this.id + ".");
		}
		return null;
	}

	public IndexerInterface getIndexer() {
		return this.indexer;
	}

	public Integer getId() {
		return this.id;
	}

	/**
	 * It allows peer to download files from other peers
	 * It first check local files list.
	 * If the file required already exists, it ignores the download request.
	 * Otherwise, it then search the indexer for available peers for downlaod.
	 * Once there are other peers available, it chooses the first one to connect to.
	 * @param filename name of file
	 * @throws RemoteException
	 */
	private void download(String filename) throws RemoteException {
		HashSet<String> current = readLocalFilenames();
		if (current.contains(filename)) {
			System.out.println("File " + filename + " already exits on Peer "
					+ getId());
			return;
		}

		// choose the source peer by the first id returned from indexer
		IndexerInterface indexer = this.getIndexer();
		Object[] objs = indexer.search(filename).toArray();
		if (objs.length == 0) {
			System.out.println("No seeds found for file " + filename);
			return;
		}

		int sourceId = (Integer) objs[0];
		PeerInterface source = null;
		try {
			source = (PeerInterface) Naming.lookup("//"
					+ indexer.getAddr(sourceId) + "/Peer" + sourceId);
			System.out.println("Peer " + this.id + " is looking up //"
					+ indexer.getAddr(sourceId) + "/Peer" + sourceId
					+ " to download.");
		} catch (Exception e) {
			System.err.println("Peer" + sourceId + " not found.");
			e.printStackTrace();
			return;
		}

		byte[] buffer = source.obtain(filename);
		writeToFile(filename, buffer);
		System.out.println("File " + filename
				+ " downloaded successfully from Peer " + sourceId + ".");

	}
	
	/**
	 * It creates a file with specified filename and writes content to it
	 * @param filename name of file
	 * @param buffer content of file
	 */
	private void writeToFile(String filename, byte[] buffer) {
		try {
			String path = "p" + this.id + "/" + filename;
			RandomAccessFile file = new RandomAccessFile(path, "rw");
			file.write(buffer);
			file.close();
		} catch (Exception e) {
			System.out.println("File " + filename + " Write Failure.");
			e.printStackTrace();
		}
	}

	/**
	 * It allows peer to register to RMI server
	 * It first read ip address, port information from local configuration file
	 * and then register.
	 */
	private void register() {
		try {
			String ip = Util.readProperties("ip");
			String basePort = Util.readProperties("basePort");
			int port = Integer.parseInt(basePort) + this.id;
			LocateRegistry.createRegistry(port);
			System.out.println("Peer " + this.id + " RMI registry created.");
			Naming.rebind("//" + ip + ":" + String.valueOf(port) + "/Peer"
					+ String.valueOf(this.id), this);
			System.out.println("Peer " + this.id + " bound to //" + ip + ":"
					+ String.valueOf(port) + "/Peer" + String.valueOf(this.id));
			this.getIndexer().regAddr(this.id, ip + ":" + String.valueOf(port));
		} catch (Exception e) {
			System.out.println("Peer " + this.id
					+ " RMI registry already exists.");
			System.out.println("Peer " + this.id + " Exit.");
			System.exit(0);
		}
	}

	/**
	 * Once peer starts, it tells indexer what files it has. Then it updates
	 * indexer every n seconds.
	 * @param n period for scheduler to execute a task
	 */
	private void scheduler(int n) {
		Timer t = new Timer();
		t.scheduleAtFixedRate(new UpdateTask(this), 0, n * 1000);
	}

	private class UpdateTask extends TimerTask {
		Peer p;

		public UpdateTask(Peer p) {
			super();
			this.p = p;
		}

		public void run() {
			try {
				p.updateIndexer();
				/*System.out.println("Peer " + getId()
						+ " updated files list on Indexer server.");*/
			} catch (Exception e) {
				System.out.println("Timer Errors.");
				e.printStackTrace();
			}
		}
	}

	/**
	 * inform the indexer about the changes
	 */
	private void updateIndexer() throws RemoteException {
		HashSet<String> current = readLocalFilenames();
		Iterator<String> c = current.iterator();
		while (c.hasNext()) {
			String s = c.next();
			if (!filenames.contains(s)) {
				filenames.add(s);
				indexer.registry(this.id, s);
			}
		}
		Iterator<String> f = filenames.iterator();
		while (f.hasNext()) {
			String s = f.next();
			if (!current.contains(s)) {
				f.remove();
				indexer.remove(this.id, s);
			}
		}
	}

	/**
	 * It reads the local files list and stores it in a hashset
	 * @return current the local files list 
	 */
	private HashSet<String> readLocalFilenames() {
		HashSet<String> current = new HashSet<String>();
		String path = System.getProperty("user.dir") + "/p" + String.valueOf(this.id);
		File directory = new File(path);
		if (directory.list() != null) {
			for (String s : directory.list())
				current.add(s);
			return current;
		} else {
			System.out.println("No local directory prepared.");
			System.out.println("Peer " + this.id + " Exit");
			System.exit(0);
		}
		return null;
	}

	/**
	 * Runnable object for multithreading
	 * 
	 */
	protected class RunDownload implements Runnable {
		private String filename;
		private Peer peer;

		protected RunDownload(Peer peer, String filename) {
			this.peer = peer;
			this.filename = filename;
		}

		@Override
		public void run() {
			try {
				peer.download(filename);
			} catch (RemoteException e) {
				e.printStackTrace();
			}
		}

	}
	
	/**
	 * It creates a thread for peer to download a file
	 * @param filename name of file
	 */
	public void spawnDownloadThread(String filename) {
		runDownload = new RunDownload(this, filename);
		Thread t = new Thread(runDownload);
		t.start();
	}

	/**
	 * The main function of peer.
	 * It starts the peer based on configuration file and register to RMI server.
	 * Then it allows user to input a filename for download.
	 * User can quit the program by input '!'
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		Peer peer = new Peer(Integer.parseInt(Util.readProperties("peerId")));
		peer.start();
		
		Scanner sc = new Scanner(System.in);
		while (true) {
			System.out
					.println("Please input the filename to download or input '!' to quit:");
			
			String input = sc.nextLine();
			
			if ("!".equals(input)) {
				sc.close();
				System.exit(0);
			}
			peer.spawnDownloadThread(input);
		}
	}
}

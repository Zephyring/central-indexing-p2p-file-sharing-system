package peer;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface PeerInterface extends Remote{
	
	public byte[] obtain(String filename) throws RemoteException;

}

package test;

import indexer.Indexer;
import peer.*;

public class TestP2P {

	/**
	 * It starts a indexer and three peers.
	 * Then each peer can download files based on the wishlist.
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		Indexer indexer = new Indexer();
		indexer.start();

		Peer[] peers = new Peer[3];
		for (int id = 0; id < 3; id++) {
			peers[id] = new Peer(id);
			peers[id].start();
		}

		String[] p0WishList = { "b", "c", "g", "h", "i", "j", "l", "n", "o",
				"p", "pic1.jpg", "zz" };
		String[] p1WishList = { "d", "e", "f", "g", "h", "i", "q", "r", "s",
				"t", "pic1.jpg", "zz" };
		String[] p2WishList = { "j", "k", "l", "m", "n", "o", "p", "q", "r",
				"s", "t", "zz" };
	
		for (String s : p0WishList) {
			peers[0].spawnDownloadThread(s);
		}
		for (String s : p1WishList) {
			peers[1].spawnDownloadThread(s);
		}
		for (String s : p2WishList) {
			peers[2].spawnDownloadThread(s);
		}
		
	}

	
}

package test;

import java.io.File;

public class CleanTest {

	public static void deleteFile(String path) {
		File file = new File(path);
		if (file.isFile() && file.exists()) {
			file.delete();
		}
	}

	/**
	 * It cleans all the files that have been downloaded
	 * and return the test case to its original state.
	 * @param args
	 */
	public static void main(String[] args) {
		String[] p0WishList = { "b", "c", "g", "h", "i", "j", "l", "n", "o",
				"p", "pic1.jpg", "zz" };
		String[] p1WishList = { "d", "e", "f", "g", "h", "i", "q", "r", "s",
				"t", "pic1.jpg", "zz" };
		String[] p2WishList = { "j", "k", "l", "m", "n", "o", "p", "q", "r",
				"s", "t", "zz" };
		for (String s : p0WishList) {
			deleteFile("p0/" + s);
		}
		for (String s : p1WishList) {
			deleteFile("p1/" + s);
		}
		for (String s : p2WishList) {
			deleteFile("p2/" + s);
		}
	}
}

package util;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Util {
	/**
	 * Read the local configuration file based on key
	 * @param key the key for your query
	 * @return value the result you look up
	 */
	public static String readProperties(String key) {
		Properties p = new Properties();
		String filePath = System.getProperty("user.dir") + "/config.properties";
		InputStream in = null;
		try {
			in = new BufferedInputStream(new FileInputStream(filePath));
			p.load(in);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (in != null)
					in.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		String value = p.getProperty(key);
		return value;
	}
}
